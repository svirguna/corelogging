﻿using System.Threading;
using System.Threading.Tasks;

namespace Extensions
{
    public class AppStartupTask : IStartupTask
    {
        private ProgramApi _api;

        public AppStartupTask(ProgramApi api)
        {
            _api = api;
        }


        public Task ExecuteAsync(CancellationToken cancellationToken = default) => _api.Start();
    }
}