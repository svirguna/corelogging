﻿using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Extensions
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder UseDefaultHosting(this IHostBuilder builder) =>
            builder
                .ConfigureHostConfiguration(hostConfig =>
                {
                    hostConfig
                        .AddJsonFile("hostsettings.json", optional: true)
                        .AddEnvironmentVariables("Host_");
                })
                .ConfigureAppConfiguration((host, appConfig) =>
                {
                    appConfig
                        .AddJsonFile("appsettings.json", optional: true)
                        .AddJsonFile($"appsettings.{host.HostingEnvironment.EnvironmentName}.json", optional: true)
                        .AddEnvironmentVariables();
                });

        public static IHostBuilder ConfigureLogger(this IHostBuilder builder)
        {
            //XmlDocument log4netConfig = new XmlDocument();
            //log4netConfig.Load(File.OpenRead("log4net.config"));
            //var repo = log4net.LogManager.CreateRepository(
            //    Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            //log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
            Logger.CreateLogger("CustomName");

            return builder;
        } 

        public static IServiceCollection AddREfDataProvider(this IServiceCollection services)
        {
            services.TryAddSingleton(provider => 
                new ReferenceDataCache(
                    provider.GetService<IConfiguration>().GetConnectionString("RedisCache")));
            services.AddStartupTask<RdcStartupTask>();

            return services;
        }

        public static async Task RunConsoleWithTasksAsync(
            this IHostBuilder hostBuilder,
            CancellationToken cancellationToken = default)
        {
            var host = hostBuilder.UseConsoleLifetime().Build();
            foreach (var startupTask in host.Services.GetServices<IStartupTask>())
                await startupTask.ExecuteAsync(cancellationToken);

            await host.RunAsync(cancellationToken);
        }

        public static IServiceCollection AddStartupTask<T>(this IServiceCollection services)
            where T : class, IStartupTask =>
            services.AddTransient<IStartupTask, T>();

        
    }
}
