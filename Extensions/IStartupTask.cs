﻿using System.Threading;
using System.Threading.Tasks;

namespace Extensions
{
    public interface IStartupTask
    {
        Task ExecuteAsync(CancellationToken cancellationToken = default);
    }
}