﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace Extensions
{
    public class Logger
    {
        private const string DefaultLogPattern = "%date %-5level [%thread]: %logger{1} - %message%newline";
        private const string DebugLogPattern = "%date %-5level [%thread]: %logger{1} - %message (%file:%line)%newline";
        private const string NonUiLogLocation = "C:\\logs";
        private static readonly Level DefaultLogLevel = Level.Trace;

        public static void CreateLogger(string baseLogName, Level logLevel = null, string env = "Development")
        {
            Configure(baseLogName, logLevel, env);
        }

        private static void Configure(string baseLogName, Level logLevel, string env)
        {
            var writeToFile = false;
            if (logLevel == null)
            {
                switch (env.ToUpper())
                {
                    case "DEVELOPMENT":
                        logLevel = Level.Debug;
                        writeToFile = true;
                        break;
                    case "PRODUCTION":
                        logLevel = Level.Info;
                        break;
                    default:
                        logLevel = DefaultLogLevel;
                        break;
                }
            }

            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetCallingAssembly());
            var tracer = new TraceAppender();
            PatternLayout patternLayout;

            if (logLevel == Level.Debug || logLevel == Level.Trace)
            {
                patternLayout = new PatternLayout { ConversionPattern = DebugLogPattern };
            }
            else
            {
                patternLayout = new PatternLayout { ConversionPattern = DefaultLogPattern };
            }

            patternLayout.ActivateOptions();

            tracer.Layout = patternLayout;
            tracer.ActivateOptions();
            hierarchy.Root.AddAppender(tracer);

            var fullyQualifiedLogFileName = Path.Combine(NonUiLogLocation, baseLogName);
            var consoleAppender = new ConsoleAppender{
                Layout = patternLayout,
            };
            consoleAppender.ActivateOptions();

            var roller = new RollingFileAppender
            {
                Layout = patternLayout,
                AppendToFile = true,
                RollingStyle = RollingFileAppender.RollingMode.Date,
                CountDirection = 1,
                StaticLogFileName = false,
                File = fullyQualifiedLogFileName
            };
            roller.ActivateOptions();
            if(writeToFile)
                hierarchy.Root.AddAppender(roller);
            hierarchy.Root.AddAppender(consoleAppender);

            hierarchy.Root.Level = logLevel;
            hierarchy.Configured = true;
        }
    }
}
