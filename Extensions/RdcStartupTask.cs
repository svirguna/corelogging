﻿using System.Threading;
using System.Threading.Tasks;

namespace Extensions
{
    public class RdcStartupTask : IStartupTask
    {
        private readonly ReferenceDataCache _rdc;

        public RdcStartupTask(ReferenceDataCache rdc)
        {
            _rdc = rdc;
        }

        public Task ExecuteAsync(CancellationToken cancellationToken = default) => _rdc.InitAsync();
    }
}