﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;
using Microsoft.Extensions.Logging;

namespace Extensions
{
    public class ReferenceDataCache
    {
        private static ILog _logger = LogManager.GetLogger(typeof(ReferenceDataCache));
        private List<int> _list = new List<int>();
        private readonly string _cs; 

        public ReferenceDataCache(string cs)
        {
            _cs = cs;
        }

        public void Add(int value)
        {
            _list.Add(value);
        }

        public int GetByPos(int pos)
        {
            if(pos < 0 || pos > _list.Count - 1)
                throw new IndexOutOfRangeException("Wrong position");
            return _list[pos];
        }

        public async Task InitAsync()
        {
            foreach (var i in new[]{1,3,5,7,9,11,13})
            {
                _list.Add(i);
            }
            _logger.Info("==========>> RDC loaded");
            await Task.FromResult(0);
        }
    }
}