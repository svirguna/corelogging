﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Extensions
{
    public class ProgramApi
    {
        private readonly ReferenceDataCache _rdc;

        public ProgramApi(ReferenceDataCache rdc)
        {
            _rdc = rdc;
        }

        public Task Start()
        {
            return Task.Factory.StartNew(() =>
            {
                Console.WriteLine("App started");
                while (true)
                {
                    var x = new Random().Next(0, 5);
                    Console.WriteLine($"Position {x} value {_rdc.GetByPos(x)}");
                    Thread.Sleep(1000);
                }
            });

        }
    }
}
