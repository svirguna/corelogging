﻿using System;
using System.Threading.Tasks;
using Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Log4net
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .UseDefaultHosting()
                .ConfigureLogger()
                .ConfigureServices((hostcontext, services) =>
                {
                    services
                        .AddREfDataProvider()
                        .TryAddSingleton<ProgramApi>();
                    services.AddStartupTask<AppStartupTask>();
                    
                });

            await builder.RunConsoleWithTasksAsync();
        }
    }
}
